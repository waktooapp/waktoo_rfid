<?php

/* Start Route Login */
Route::get('/', 'AuthController@index');
Route::post('/login', 'AuthController@postLogin');
Route::get('/logout', 'AuthController@logout');
/* End Route Login */

/* Start Route Dashboard */
Route::get('/maps', 'MapsController@index');
/* End Route Dashboard */