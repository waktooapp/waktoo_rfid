<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Waktoo - RFID</title>
    <!-- Favicon-->
    <link rel="icon" href="http://waktoo.com/assets/images/icon/4.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">{{ config('app.name') }}<b>RFID</b></a>
            <small>Tracking Everyone Anywhare</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="loginForm" method="POST" action="javascript:void(0);" data-toggle="validator">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-primary waves-effect" type="submit">LOGIN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/sign-in.js"></script>

    <script type="text/javascript">
        var path = '{{ config("app.url") }}';
         $(document).ready(function() {
            $('#loginForm').on('submit',(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    method  : 'POST',
                    url     : "{{ url('login') }}",
                    data    : formData,
                    contentType: false,
                    processData: false,
                    success: function(data, status, xhr) {
                        var result = JSON.parse(xhr.responseText);
                        if (result.status == true) {
                            $('#alert_dis').html('<div class="alert success"><span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> '+result.message+'</div>');
                            window.location = path;
                        } else {
                            $('#alert_dis').html('<div class="alert"><span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> '+result.message+'</div>');
                        }
                    },
                    error: function(data) {
                        $('#alert_dis').html('<div class="alert"><span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span> Terjadi kesalahan sistem</div>');
                    }
                });
                }));
            // setInterval(ajaxCall, 10000); //300000 MS == 5 minutes
            // function ajaxCall() {
            //     alert('haha');
            // }
        });
    </script>
</body>

</html>