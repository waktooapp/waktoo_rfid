@extends('layouts.master')
@section('title')
    Maps - RFID
@endsection
@section('content')
    <section class="content">
            <div class="row">
                <button class="btn btn-default collapsible" style="position: absolute; z-index:2; right: 24px; margin-top: 10px">
                    <img src="https://png.pngtree.com/svg/20170915/4ad49ce59d.svg" style="width: 20px">
                </button>
                <div class="content" style="display: none;overflow: hidden; z-index:2; background-color: white; position: fixed; right: 24px; width: 120px; margin-top: 40px; padding: 0 5px 0 9px;border-radius: 25px; border: 2px solid black;" >            
                    <form>
                        <div class="checkbox">
                            <input type="checkbox" id="basic_checkbox_1" onclick="hilang3()" checked />
                            <label for="basic_checkbox_1">Pimpinan</label>
                        </div> 
                        <div class="checkbox">
                            <input type="checkbox" id="basic_checkbox_2" onclick="hilang2()" checked />
                            <label for="basic_checkbox_2">Staf</label>
                        </div> 
                        <div class="checkbox">
                            <input type="checkbox" id="basic_checkbox_3" onclick="hilang()"checked />
                            <label for="basic_checkbox_3">Pengunjung</label>
                        </div>    
                    </form>
                </div>
            </div>
        <div class="container-fluid" id="map" style="width:100%; height:600px; z-index:1;" >          
        </div>
    </section>
@endsection

@section('cssPlugin')
    <link rel="stylesheet" type="text/css" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.css" />
    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.Default.css" />
@endsection

@section('jsPlugin')
    <script type='text/javascript' src='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'></script>
    <script type='text/javascript' src='http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/leaflet.markercluster.js'></script>
    <script>
        var pimpinan = [];
        var staff = [];
        var visitor = [];
        var center = [-6.876795, 107.587794];
        var map = L.map('map', {zoomControl:false}).setView([-6.875896, 107.588019], 18);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            minZoom:17,
            maxZoom:18
        }).addTo(map);

        var StaffIcon = L.icon({
            iconUrl: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|abcdef&chf=a,s,ee00FFFF'
        });
        var LeaderIcon = L.icon({
            iconUrl: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|e85141&chf=a,s,ee00FFFF'
        });
        var VisitoeIcon = L.icon({
            iconUrl: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|e85141&chf=a,s,ee00FFFF'
        });

        var popup = L.popup();

        var markerClusters = L.markerClusterGroup({spiderfyOnMaxZoom: false});
        var markerClusterV = L.markerClusterGroup({spiderfyOnMaxZoom: false});
        var markerClusterP = L.markerClusterGroup({spiderfyOnMaxZoom: false});

        visitorMarker();
        staffMarker();
        pimpinanMarker();

        var imageUrl = 'images/denah.jpg',
        imageBounds = [[-6.874266, 107.585509], [-6.877610, 107.590521]];

        L.imageOverlay(imageUrl, imageBounds, {
            opacity: 0.8,
            alt: "Denah ruangan"
        }).addTo(map);

        function hilang(){
            if (document.getElementById("basic_checkbox_3").checked) {
                // checklist
                visitorMarker();
            }else{
                // uncchecklist
                // popup.remove(map);
                map.closePopup();
                closure(markerClusterV);
                markerClusterV = L.markerClusterGroup({spiderfyOnMaxZoom: false});
            }
        }
        function hilang2(){
            if (document.getElementById("basic_checkbox_2").checked) {
                // checklist
                staffMarker();
            }else{
                // uncchecklist
                closure(markerClusters);
                markerClusters = L.markerClusterGroup({spiderfyOnMaxZoom: false});
            }
        }

        function hilang3(){
            if (document.getElementById("basic_checkbox_1").checked) {
                // checklist
                pimpinanMarker();
            }else{
                // uncchecklist
                closure(markerClusterP);
                markerClusterP = L.markerClusterGroup({spiderfyOnMaxZoom: false});
            }
        }    

        function visitorMarker(){
            for (var i = 0; i < 6; i++) {
                markerClusterV.addLayer(L.marker([-6.876865, 107.587562],{icon: VisitoeIcon}).bindPopup('Visitor'+i));
                map.addLayer(markerClusterV);
            }
            
            markerClusterV.on('clusterclick', function(a){
                popup
                    .setLatLng(a.latlng)
                    .setContent("<center><b>Ruang Tunggu</b></center><table class='table'>    <thead><tr><th>Nama</th><th>Status</th></tr></thead><tbody><tr><td>Mark</td><td>Visitor</td></tr><tr><td>Jacob</td><td>Visitor</td></tr><tr><td>Larry</td><td>Visitor</td></tr></tbody></table>")
                    .openOn(map);
            });
        }

        function staffMarker(){
            for (var i = 0; i < 10; i++) {
                markerClusters.addLayer(L.marker([-6.875549, 107.587329],{icon: StaffIcon}).bindPopup('staff'+i));
                map.addLayer(markerClusters);
            }

            markerClusters.on('clusterclick', function(a){
                popup
                    .setLatLng(a.latlng)
                    .setContent("<center><b>Ruang Staff</b></center><table class='table'>    <thead><tr><th>Nama</th><th>Status</th></tr></thead><tbody><tr><td>Mark</td><td>Staff</td></tr><tr><td>Jacob</td><td>Staff</td></tr><tr><td>Larry</td><td>Staff</td></tr></tbody></table>")
                    .openOn(map);
            });
        }
        function pimpinanMarker(){
            for (var i = 0; i < 2; i++) {
                markerClusterP.addLayer(L.marker([-6.875492, 107.589387],{icon: LeaderIcon}).bindPopup('Pimpinan'+i));
                map.addLayer(markerClusterP);
            }

            markerClusterP.on('clusterclick', function(a){
                popup
                    .setLatLng(a.latlng)
                    .setContent("<center><b>Ruang Pimpinan</b></center><table class='table'>    <thead><tr><th>Nama</th><th>Status</th></tr></thead><tbody><tr><td>Mark</td><td>CEO</td></tr><tr><td>Jacob</td><td>CTO</td></tbody></table>")
                    .openOn(map);
            });
        }        

        //hide marker
        function closure(marker){
            map.removeLayer(marker);
        }
    </script>
    <script>
        var coll = document.getElementsByClassName("collapsible");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.display === "block") {
                    content.style.display = "none";
                } else {
                    content.style.display = "block";
                }
            });
        }
    </script>
@endsection
