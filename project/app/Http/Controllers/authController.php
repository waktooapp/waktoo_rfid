<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        /* set session */
        if (Session::get('login')) {
            return view('dashboard');
        }else{
            return view('login');
        }
    }

    public function postLogin(Request $request)
    {
        /* declare paramener */
        $username = $request->username;
        $password = md5($request->password);

        /* check username on database */
        $user = User::where('email_user', $username)->first();

        if ($user) {
            /* check password */
            if ($user->password_user == $password) {
                /* set true password */
                /* set session */
                Session::put('nip', $user->nip);
                Session::put('nama', $user->nama_user);
                Session::put('foto', $user->foto_user);
                Session::put('level', 'administrator');
                Session::put('login', TRUE);

                $result['status'] = true;
                $result['message'] = 'Success.';
                $result['data'] = array();
            }else{
                /* set false password */
                $result['status'] = false;
                $result['message'] = 'Username / Password yang anda masukkan salah.';
                $result['data'] = array();
            }
        }else{
            $result['status'] = false;
            $result['message'] = 'Username yang anda masukkan belum terdaftar.';
            $result['data'] = array();
        }

        return json_encode($result);
    }

    public function logout()
    {
        /* delete all session */
        Session::flush();
        return redirect('/');
    }
}
